# README #

This repository is part of the bachelorthesis project of Carlo Bernardini, Cultural information studies student at the University of Asmterdam.
It contains a library for structuring requests to the new Europeana API, a PHP controller which calls functions in the library and a client application for rendering objects from the API on a canvas graph.

### Contents ###

* Experimental PHP-library for communication with the Europeana v2 API
* * europeana.class.php
* Client application for rendering Europeana records in graph visualization
* * graph.html / graph.js
* Customized ArborJS renderer
* * renderer.js