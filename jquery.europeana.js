$(function(){
	var Europeana = function (key) {
		var apikey = key,
			main = this,
			uri = null;
			httpstatus = function (code) {
				var codes = {
					200: 'The request was executed successfully.',
					401: 'Authentication credentials were missing or authentication failed.',
					404: 'The requested record was not found.',
					429: 'The request could be served because the application has reached its usage limit.',
					500: 'Internal server error.'
				};
				return codes[code];
			},
			buildhost = function (params) {
				var cfg = {
					endpoint: 'search',
					id: null
				}, endpoint;
				cfg = $.extend({}, cfg, params);
				if (cfg.endpoint in main.host.endpoints) {
					endpoint = main.host['base'] + main.host['endpoints'][cfg.endpoint];
					if (cfg.id !== null) endpoint = endpoint.replace('[id]', cfg.id);
					return endpoint;
				}
				return false;
			},
			fetch = function (u) {
				if (typeof u == 'undefined') u = uri;
				$.ajax({
					url: u,
					dataType: 'jsonp',
					success: function(response){
						
						console.log (response);
					}
				});
			}
		this.vars = {
				facets: ['COUNTRY', 'DATA_PROVIDER', 'LANGUAGE', 'PROVIDER', 'RIGHTS', 'TYPE', 'UGC', 'YEAR'],
				profiles: ['standard', 'minimal', 'facets', 'breadcrumbs', 'portal']
			};
		this.host = {
				base: 'http://europeana.eu/api/v2',
				endpoints: {
					search: '/search.json',
					providers: '/providers.json',
					datasets: '/provider/[id]/datasets.json',
					record: '/record/[id].json',
					provider: '/provider/[id].json',
					dataset: '/dataset/[id].json'
				}
			};
		this.test = function(){
			return buildhost({ endpoint: 'record', id: 23213214});
		};
		this.getRecord = function(params) {
			var cfg = {
				id: null,
				callback: false
			}, host;
			cfg = $.extend({}, cfg, params);
			if (!cfg.id) return false;
			uri = buildhost({ endpoint: 'record', id: cfg.id }) + '?wskey=' + apikey;
			if (typeof callback != 'undefined')  uri += '&callback=' + encodeURIComponent(callback);
			fetch();
		}
	}

	var E = new Europeana('MmH8NPjhj');
	E.getRecord({ id: '\/04802\/3F01774036ADF624097AB05C25B3AD0B0062E706' });
});