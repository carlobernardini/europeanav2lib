<?php
header("Content-type: application/json");
require ('europeana.class.php');
$europeana = new Europeana('MmH8NPjhj');

// echo $europeana->getDataset(2021608);
// echo $europeana->fetchExtSource("http://amdata.adlibsoft.com/wwwopac.ashx?database=AMcollect&search=title=Trembleuse&xmltype=grouped&limit=1&output=json");
// echo $europeana->getRecord('/2021608/dispatcher_aspx_action_search_database_ChoiceCollect_search_priref_12974');
// $results = $europeana->search(array(
		// 'query' => 'Trembleuse',
		// 'qf' => array(
			// 'data_provider' => '"Amsterdam Museum"',
		// ),
		// 'rows' => 100,
		// 'blablabla' => 'test'
	// ));
// $results = json_decode($results, true);
// $records = array();
// foreach ($results['items'] as $result) {
	// echo $europeana->getRecord($result['id']);
// }
// echo $europeana->getRecord("/2021608/dispatcher_aspx_action_search_database_ChoiceCollect_search_priref_11923");
// exit;

if (isset($_GET['action'])) {
	switch ($_GET['action']) {
		case "search":
			$response = $europeana->search($_GET);
		break;
		
		case "record":
			$id = (isset($_GET['id'])) ? trim($_GET['id']) : false;
			if (!$id) {
				$response = array(
					'status'=>'error',
					'msg'=>'No id given'
				);
			} else {
				$callback = (isset($_GET['callback'])) ? trim($_GET['callback']) : false;
				$similar = (isset($_GET['similar'])) ? true : false;
				$response = $europeana->getRecord($id, $similar, $callback);
			}
		break;
		
		case "suggestions":
			$query = (isset($_GET['q'])) ? trim($_GET['q']) : false;
			if (!$query) {
				$response = array(
					'status' => 'error',
					'msg' => 'No query given'
				);
			} else {
				$response = $europeana->getSuggestions($query);
			}
		break;
		
		case "modal":
			$modal = <<<EOT
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4 class="modal-title" id="myModalLabel">{{title}}</h4>
</div><!-- .modal-header-->
<div class="modal-body">
	{{msg}}
</div><!-- .modal-body -->
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div><!-- .modal-footer-->
EOT;
			$search  = array("{{title}}", "{{msg}}");
			switch ($_GET['msg']) {
				case "noresult":
					$replace = array("No results", "No matches found for the current query");
				break;
				case "exception_fetch":
					$replace = array("Exception encoutered", "An exception occurred when fetching records from the API, some objects may be omitted from the graph");
				break;
				case "noquery":
					$replace = array("No search terms provided", "Please enter a search query in order to plot a graph");
				break;
			}
			$response = str_replace($search, $replace, $modal);
		break;
	}
} else {
	$response = array(
		'status'=>'error',
		'msg'=>'No action requested'
	);
}
if (isset($response) && is_array($response)) $response = json_encode($response, JSON_PRETTY_PRINT);
if (isset($response) && $europeana->is_json($response)) header("Content-type: application/json");
print $response;
?>