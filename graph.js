$(function(){
	console.log ('engine started');
	
	var records = [],
		sys = arbor.ParticleSystem({
			repulsion: 100,
			stiffness: 0,
			fps: 1
		}),
		recursion = 1,
		el = {
			viewport: $('#viewport'),
			queryfield: $('#query'),
			hint: $('#hint'),
			modal: $('#modal'),
			datapanel: {
				panel: $('#datapanel'),
				values: $('#datapanel-values')
			},
			progress: {
				container: $('#progress'),
				value: $('#progress-value')
			},
		},
		exception = false,
		edges = [],
		lvlColors = {0: '#A95D38', 1: '#CD885A', 2: '#F5BC8B'},
		uritpl = 'http://europeana.eu/portal/record/[id].html',
		regex = {
			url: /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/,
			queryWho: /^(who|what|where):\"([a-z0-9\s]+){1}(\"?)/i,
			cleanStr: /[^\w\s]/gi
		}
		requests = { done: 0, total: 0, pct: 0 },
		hintShown = {
			showRecordData: false,
			extraQuery: false
		},
		plotRequested = false;
	
	el.viewport.attr({ width: function(){
			return $(this).parent().width();
		}, height: function(){
			$(this).parent().css('height', $(window).height()-70);
			return $(this).parent().height();
		}	
	});
	
	el.queryfield.bind('focus keyup click', function(){
		el.queryfield.popover('destroy');
	});
	
	el.datapanel['panel'].css({
		position: 'fixed',
		bottom: 20,
		left: 20,
		width: 450
	});
	
	var _is = function ($type, $thing) {
			return (typeof $thing == $type);
		},
		_isnot = function ($type, $thing) {
			return !(_is($type, $thing));
		}
	
	
	var showRecordData = function (record) {
		if (el.hint.is(':visible')) el.hint.fadeOut(200);
		var obj = records[record].data.object,
			tbl = [];
		tbl.push("<tr><td colspan=\"2\"><a href=\"" + records[record].link +"\" target=\"_blank\">View object in Europeana portal</a>&nbsp;<i class=\"fa fa-external-link\"></i></td></tr>");
		for (entry in obj.proxies[0]) {
			var titled = false,
				title;
			for (def in obj.proxies[0][entry]) {
				if (_isnot('undefined', obj.proxies[0][entry]['def'])) {
					title = entry.replace('dc', '');
					value = obj.proxies[0][entry]['def'][0];
					if (value.match(regex.url)) value = '<a href="'+value+'" target="_blank">'+value+'</a>&nbsp;<i class="fa fa-external-link"></i>';
					tbl.push("<tr><th>"+((titled) ? "" : title)+"</th><td>"+value+"</td></tr>");
					titled = true;
				}
			}
		}
		tbl = tbl.join("\n");
		el.datapanel['values'].html(tbl);
		el.datapanel['panel'].show();
		el.datapanel['panel'].on('click', 'a.close', function(){
			el.datapanel['panel'].hide();
			return false;
		});
	}
	
	var updateProgress = function(){
		var pct = requests.done / requests.total * 100;
		if (pct < requests.pct) pct = requests.pct;
		requests.pct = pct;
		el.progress['value'].attr('aria-valuenow', requests.pct).css('width', requests.pct+'%');
	}
	
	var shorten = function (str) {
		str = String(str);
		if (_is('undefined', str) || str.length < 40) return str;
		var strStart  = str.substr(0, 20),
			strLength = str.length,
			strEnd    = str.substr(strLength - 20, strLength);
		return (strStart + '...' + strEnd);
	}
	
	var plot = function(){
		if (!hintShown.showRecordData) {
			el.hint.fadeIn(200);
			hintShown.showRecordData = true;
		}
		if (!hintShown.extraQuery) {
			el.queryfield.popover({
				placement: 'bottom',
				content: 'Objects from all subsequent queries will be added to the current graph',
				trigger: 'manual'
			});
			el.queryfield.popover('show');
			setTimeout(function(){
				el.queryfield.popover('destroy');
			}, 5000);
			hintShown.extraQuery = true;
		}
		var title;
		for (record in records) {
			if (_isnot('undefined', records[record].data.object)) {
				var a = 0;
				for (i in records[record].data.object.proxies[0]['dcTitle']) {
					if (a === 0) title = records[record].data.object.proxies[0]['dcTitle'][i];
					a++;
				}
			}
			sys.addNode(record, {
				label: (_isnot('undefined', title)) ? shorten(title) : 'Untitled object',
				color: lvlColors[records[record].level],
				link: records[record].link,
				callback: showRecordData,
				img: records[record].img,
				imgSmall: records[record].imgSmall
			});
		}
		for (edge in edges) {
			sys.addEdge(edges[edge].from, edges[edge].to);
		}
		sys.renderer = Renderer('#viewport');
		if (exception) { 
			el.modal.modal({
				remote: 'controller.php?action=modal&msg=exception_fetch',
				show: true
			});
			exception = false;
		}
	}
	var getRecord = function(params) {
		var cfg = {
			img: null,
			imgSmall: false,
			id: '',
			parent: false,
			level: 0,
			maxRecursion: recursion
		}
		
		cfg = $.extend({}, cfg, params);
		if (cfg.maxRecursion > 3) cfg.maxRecursion = 3;
		
		requests.total++;
		$.ajax({
			url: 'controller.php',
			data: {
				action: 'record',
				id: cfg.id,
				similar: true,
			},
			dataType: 'json',
			success: function (record) {
				try {
					if (!(cfg.id in records)) {
						if (_is('undefined', record.object)) console.log (record);
						if (cfg.img == null && _isnot('undefined', record.object.europeanaAggregation.edmPreview)) {
							cfg.img = String(record.object.europeanaAggregation.edmPreview);
							cfg.imgSmall = true;
						}
						records[cfg.id] = { 
							data: record,
							level: cfg.level,
							link: uritpl.replace('[id]', cfg.id),
							img: (typeof cfg.img !== null) ? cfg.img : false,
							imgSmall: cfg.imgSmall
						};
					}
					if (cfg.parent) {
							edges.push({ from: cfg.parent, to: cfg.id });
						}
					if (typeof record.similarItems !== 'undefined' && (cfg.level < cfg.maxRecursion)) {
						for (obj in record.similarItems) {
							getRecord({ id: record.similarItems[obj].id, parent: cfg.id, level: cfg.level + 1 });
						}
					}
				} catch (e) {
					exception = true;
				}
			}
		});
	}
	$(document).ajaxStop(function(){
		el.progress['container'].hide();
		if (plotRequested) {
			console.log ('all pending requests have been executed');
			plot();
			plotRequested = false;
		}
	});
	$(document).ajaxComplete(function(){
		requests.done++;
		updateProgress();
	});
	$('form#plot').submit(function(e){
		e.preventDefault();
		$('#query').typeahead('close');
		if (el.queryfield.val().trim() == '') return el.modal.modal({
			remote: 'controller.php?action=modal&msg=noquery',
			show: true
		});
		
		plotRequested = true;
		var form = $(this);
			data = form.serializeArray();
		data.push ({
			qf: { TYPE: 'IMAGE' }
		});
		records = [];
		edges = [];
		requests.done = 0;
		requests.pct = 0;
		requests.total = 1;
		updateProgress();
		el.progress['container'].show();
		$.ajax({
			url: 'controller.php',
			data: data,
			dataType: 'json',
			success: function (data) {
				console.log ('start fetching records');
				if (data.itemsCount > 0) {
					var num = data.items.length,
						cnt = 0,
						cfg = {};
					for (obj in data.items) {
						cfg.id = data.items[obj].id;
						cfg.img = (typeof data.items[obj].edmPreview !== 'undefined') ? String(data.items[obj].edmPreview[0]) : null;
						getRecord(cfg);
					}
				} else {
					plotRequested = false;
					el.modal.modal({
						remote: 'controller.php?action=modal&msg=noresult',
						show: true
					});
				}
			},
			error: function(){
				console.log ('Not connected');
			}
		});
		return false;
	});
	
	var normalizeString = function (str) {
		var m = str.match(regex.queryWho);
		if (m !== null && m.length) str = m[2];
		return str.replace(regex.cleanStr, '').toLowerCase().trim();
	}
	
	var suggestion = {
		input: '',
		normalized: ''
	};
	var suggestions = new Bloodhound({
		datumTokenizer: function (d){
		
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: 'controller.php?action=suggestions&q=',
			replace: function (url, query) {
				suggestion.input = query;
				suggestion.normalized = normalizeString(query);
				return url + suggestion.normalized;
			},
			filter: function (data) {
				if (data.itemsCount == 0) return [];
				return $.map(data.items, function(item){
					var m = suggestion.input.match(regex.queryWho);
					return {
						value: (m !== null && m.length) ? m[1] + ':"'+item.term+'"' : item.term
					}
				});
			}
		}
	});
	suggestions.initialize();
		
	$('#query').typeahead({
		minLength: 2,
		highlight: true
	},{
		displayKey: 'value',
		source: suggestions.ttAdapter()
	});
	$('form').on('click', 'a.submit', function(){
		$(this).closest('form').submit();
		return false;
	}).on('keypress', function(e){
		if (e.which == 13) { 
			e.preventDefault();
			$(this).submit();
		}
	});
});